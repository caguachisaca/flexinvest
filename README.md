# Task React Native

App ios and android development with react-native

## Get Started

### Installation:

Get the code

```
$ git clone https://caguachisaca@bitbucket.org/caguachisaca/flexinvest.git
```
enter the folder

```
cd flexinvest
```
Install dependencies

```
npm install
```

Run in IOS

```
react-native run-ios
```
Run in Android

```
react-native run-android
```

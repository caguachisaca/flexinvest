import React, {Component} from 'react';

import {Router, Scene} from 'react-native-router-flux';

import Login from './views/Login';
import Home from './views/Home';

export default class App extends Component {
    render() {
        return (
            <Router getSceneStyle={getSceneStyle}>
                <Scene key='root'>
                    <Scene key="login" component={Login} hideNavBar/>
                    <Scene key="home" component={Home} hideNavBar/>
                </Scene>
            </Router>
        );
    }
}

const getSceneStyle = (/* NavigationSceneRendererProps */ props, computedProps) => {
    const style = {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null
    }
    if (computedProps) {
        style.marginTop = computedProps.hideNavBar ? 0 : 50
        style.marginBottom = computedProps.hideTabBar ? 0 : 0
    }
    return style
}

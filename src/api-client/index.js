const parameters = require('../parameters')
const URL = `${parameters.URL}`

async function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response.json();
    } else {
        let message
        await response.json().then(body => {
            message = body.message
        })
            .catch(err => {
                message = 'Error URL mal formado'
            })
        let error = new Error(message)
        error.statusCode = response.status
        throw error
    }
}


function signIn(login) {
    return fetch(`${URL}/auth/`, {
        method: 'POST',
        headers: parameters.getHeader(),
        body: JSON.stringify(login)
    }).then(checkStatus)
}

function getStrategies() {
    return fetch(`${URL}/strategies`, {
        headers: parameters.getHeader(),
    }).then(checkStatus)
}

export {
    signIn,
    getStrategies
}
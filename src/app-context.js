import React, {Component} from 'react';

type Props = {};
export const AppContext = React.createContext();
export const AppConsumer = AppContext.Consumer;

export class AppProvider extends Component {

    constructor(props) {
        super(props);

        this.state = {
            token: ''
        }
    }

    setToken = (token) => {
        this.setState({token});
    }

    render() {
        return (
            <AppContext.Provider value={{
                token: this.state.token,
                setToken: this.setToken
            }}>
                {this.props.children}
            </AppContext.Provider>
        );
    }
}


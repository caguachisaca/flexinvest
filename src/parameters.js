module.exports = {
    USER: '',
    URL: 'https://flexinvest.me/drf',
    TOKEN: '',
    getHeader: function () {
        return new Headers({
            'Authorization': `Bearer ${this.TOKEN}`,
            'Content-Type': 'application/json'
        })
    }
}

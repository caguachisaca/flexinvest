import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert,
    FlatList,
    ActivityIndicator
} from 'react-native';
import {List, ListItem} from 'react-native-elements';
import {getStrategies} from '../api-client';

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: []
        }
    }

    componentDidMount() {
        getStrategies()
            .then(data => {
                this.setState({
                    loading: false,
                    data
                });
            })
            .catch(err => {
                if (err.statusCode === 400) Alert.alert('Error', 'Error loading data.');
            })
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator size="large" color="#0000ff"/>
                    <Text>
                        Cargando Datos
                    </Text>
                </View>
            )
        }
        return (
            <View>
                <List>
                    <FlatList
                        data={this.state.data}
                        renderItem={({item}) => (

                            <ListItem
                                roundAvatar
                                title={item.name}
                                subtitle={item.description}
                                avatar={item.thumbnail_150}
                            />

                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </List>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    list: {
        flex: 1,
        paddingTop: 50,
        paddingLeft: 5
    }
});
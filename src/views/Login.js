import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    Alert
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Text, Button, Divider} from 'react-native-elements';
import {signIn} from '../api-client';
import parameters from '../parameters'

type Props = {};
export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false
        }
    }

    handleLogin() {
        let user = this.refs.user._lastNativeText;
        let pass = this.refs.pass._lastNativeText;

        this.setState({
            loading: true
        });

        let login = {
            "username": user,
            "password": pass
        }
        // let login = {
        //     "username": "testus7@gmail.com",
        //     "password": "Test1234"
        // }

        signIn(login)
            .then(data => {
                this.setState({loading: false});
                parameters.TOKEN = data.token
                Actions.home();
            })
            .catch(err => {
                if (err.statusCode === 400) {
                    this.setState({loading: false});
                    Alert.alert('Error', 'Unable to log in with provided credentials.');
                }
            })
    }


    render() {
        return (
            <View style={styles.container}>
                <Text h1>FlexInvest</Text>
                <TextInput ref='user' style={styles.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder='Ingrese Usuario'
                />

                <TextInput ref='pass' style={styles.inputBox}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder='Ingrese Contraseña'
                           secureTextEntry={true}
                />

                <Divider style={{backgroundColor: 'blue', height: Platform.OS === 'ios' ? 5 : 10}}/>

                <Button backgroundColor="#2B98F0" style={styles.button} onPress={this.handleLogin.bind(this)}
                        raised loading={this.state.loading}
                        title='Sign in'/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    inputBox: {
        width: 300,
        backgroundColor: 'gainsboro',
        paddingHorizontal: 16,
        fontSize: 16,
        marginTop: 10,
        paddingVertical: 15
    },
    textButton: {
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'center',
        color: 'yellow'
    },

    button: {
        //marginTop: Platform.OS === 'ios' ? 200 : 100,
        marginTop: 12,
        width: 100,
    },

});

